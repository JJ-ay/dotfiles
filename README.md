# Dotfiles

Repository for my set of dotfiles. i was inspired by ![this dude](https://github.com/addy-dclxvi/almighty-dotfiles) and his "i3 Vacant" theme and tried reproducing it in Sway. Below there's a preview of how it turned out:

![](https://gitlab.com/JJ-ay/dotfiles/-/raw/main/images/Pictures/preview.png)
