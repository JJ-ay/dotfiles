# for color cheatsheet check:
# https://www.ditig.com/256-colors-cheat-sheet

from __future__ import (absolute_import, division, print_function)

from ranger.gui.colorscheme import ColorScheme
from ranger.gui.color import (
    black, blue, cyan, green, magenta, red, white, yellow, default,
    normal, bold, reverse, dim, BRIGHT,
    default_colors,
)


class Default(ColorScheme):
    progress_bar_color = 160

    def use(self, context):  # pylint: disable=too-many-branches,too-many-statements
        fg, bg, attr = default_colors

        if context.reset:
            return default_colors

        elif context.in_browser:
            if context.selected:
                attr = reverse
            else:
                attr = normal
            if context.empty or context.error:
                bg = 160
            if context.border:
                fg = default
            if context.media:
                if context.image:
                    fg = 178
                else:
                    fg = 133
            if context.container:
                fg = 160
            if context.directory:
                attr |= bold
                fg = 160
                fg += BRIGHT
            elif context.executable and not \
                    any((context.media, context.container,
                         context.fifo, context.socket)):
                attr |= bold
                fg = 92
#                fg += BRIGHT
            if context.socket:
                attr |= bold
                fg = 133
                fg += BRIGHT
            if context.fifo or context.device:
                fg = 178
                if context.device:
                    attr |= bold
                    fg += BRIGHT
            if context.link:
                fg = 197 if context.good else 133
            if context.tag_marker and not context.selected:
                attr |= bold
                if fg in (160, 133):
                    fg = white
                else:
                    fg = red
                fg += BRIGHT
#            if context.line_number and not context.selected:
#                fg = default
#                attr &= ~bold
            if not context.selected and (context.cut or context.copied):
                attr |= bold
                fg = black
                fg += BRIGHT
                # If the terminal doesn't support bright colors, use dim white
                # instead of black.
                if BRIGHT == 0:
                    attr |= dim
                    fg = white
            if context.main_column:
                # Doubling up with BRIGHT here causes issues because it's
                # additive not idempotent.
                if context.selected:
                    attr |= bold
                if context.marked:
                    attr |= bold
                    fg = 178
            if context.badinfo:
                if attr & reverse:
                    bg = 133
                else:
                    fg = 133

            if context.inactive_pane:
                fg = 152

        elif context.in_titlebar:
            if context.hostname:
                fg = white if context.bad else 160
            elif context.directory:
                fg = white
            elif context.tab:
                if context.good:
                    bg = white
                    fg = black
            elif context.link:
                fg = 152
            attr |= bold

        elif context.in_statusbar:
            if context.permissions:
                if context.good:
                    fg = 152
                elif context.bad:
                    fg = 133
            if context.marked:
                attr |= bold | reverse
                fg = 178
                fg += BRIGHT
            if context.frozen:
                attr |= bold | reverse
                fg = 152
                fg += BRIGHT
            if context.message:
                if context.bad:
                    attr |= bold
                    fg = 160
                    fg += BRIGHT
            if context.loaded:
                bg = self.progress_bar_color
            if context.vcsinfo:
                fg = 110
                attr &= ~bold
            if context.vcscommit:
                fg = 178
                attr &= ~bold
            if context.vcsdate:
                fg = 152
                attr &= ~bold

        if context.text:
            if context.highlight:
                attr |= reverse

        if context.in_taskview:
            if context.title:
                fg = 110

            if context.selected:
                attr |= reverse

            if context.loaded:
                if context.selected:
                    fg = self.progress_bar_color
                else:
                    bg = self.progress_bar_color

        if context.vcsfile and not context.selected:
            attr &= ~bold
            if context.vcsconflict:
                fg = 133
            elif context.vcsuntracked:
                fg = 152
            elif context.vcschanged:
                fg = 160
            elif context.vcsunknown:
                fg = 160
            elif context.vcsstaged:
                fg = 110
            elif context.vcssync:
                fg = 110
            elif context.vcsignored:
                fg = default

        elif context.vcsremote and not context.selected:
            attr &= ~bold
            if context.vcssync or context.vcsnone:
                fg = 110
            elif context.vcsbehind:
                fg = 160
            elif context.vcsahead:
                fg = 110
            elif context.vcsdiverged:
                fg = 133
            elif context.vcsunknown:
                fg = 160

        return fg, bg, attr
