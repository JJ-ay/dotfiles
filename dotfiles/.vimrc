  runtime! archlinux.vim
  
" -----------------------
" Funkcje
" -----------------------

fun! ToggleCC()
  if &cc == ''
    set tw=105
    set cc=107
  else
    set tw=0
    set cc=
  endif
endfun

command! -nargs=? -range Dec2hex call s:Dec2hex(<line1>, <line2>, '<args>')
function! s:Dec2hex(line1, line2, arg) range
  if empty(a:arg)
    if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
      let cmd = 's/\%V\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
    else
      let cmd = 's/\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
    endif
    try
      execute a:line1 . ',' . a:line2 . cmd
    catch
      echo 'Error: No decimal number found'
    endtry
  else
    echo printf('%x', a:arg + 0)
  endif
endfunction

command! -nargs=? -range Hex2dec call s:Hex2dec(<line1>, <line2>, '<args>')
function! s:Hex2dec(line1, line2, arg) range
  if empty(a:arg)
    if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
      let cmd = 's/\%V0x\x\+/\=submatch(0)+0/g'
    else
      let cmd = 's/0x\x\+/\=submatch(0)+0/g'
    endif
    try
      execute a:line1 . ',' . a:line2 . cmd
    catch
      echo 'Error: No hex number starting "0x" found'
    endtry
  else
    echo (a:arg =~? '^0x') ? a:arg + 0 : ('0x'.a:arg) + 0
  endif
endfunction

" -----------------------
" Ustawienia do vim-plug
" 	help: https://github.com/junegunn/vim-plug
" -----------------------
  call plug#begin('~/.vim/plugged')
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
  Plug 'preservim/nerdtree'
  Plug 'preservim/tagbar'
  Plug 'jiangmiao/auto-pairs'
  Plug 'chrisbra/Colorizer'
  Plug 'octol/vim-cpp-enhanced-highlight'


  call plug#end()
" -----------------------
" Przydatne skróty klawiszowe do pisania kodu w większym projekcie
" -----------------------
  nmap <F3> :tabnew<CR>
  nmap <F4> :tabclose<CR>
  nmap <F7> :call ToggleCC()<CR>
  nmap <F8> :NERDTree<bar>TagbarOpen<CR>
  nmap <F9> :NERDTreeClose<bar>TagbarClose<CR>
  nmap <F12> :hi<space>Comment<space>ctermfg=grey<CR>
  nmap <C-h> :tabprevious<CR>
  nmap <C-l> :tabnext<CR>

" -----------------------
" CoC 
" -----------------------
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set noswapfile
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use to navigate diagnostics
" Use :CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> <F1> <Plug>(coc-diagnostic-prev)
nmap <silent> <F2> <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> <space>d :call<space>CocAction('jumpDefinition','tab<space>drop')<CR>
nmap <silent> <space>D :call<space>CocAction('jumpDeclaration','tab<space>drop')<CR>
nmap <silent> <space>y :call<space>CocAction('jumpTypeDefinition','tab<space>drop')<CR>
nmap <silent> <space>i :call<space>CocAction('jumpImplementation','tab<space>drop')<CR>
nmap <silent> <space>r :call<space>CocAction('jumpReferences','tab<space>drop')<CR>
nmap <silent> <space>f :call<space>CocFix<CR>
"nmap <silent> <space>d <Plug>(coc-definition)
"nmap <silent> <space>D <Plug>(coc-declaration)
"nmap <silent> <space>y <Plug>(coc-type-definition)
"nmap <silent> <space>i <Plug>(coc-implementation)
"nmap <silent> <space>r <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocActionAsync('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" -----------------------
" Potrzebne do live-latex-preview
" -----------------------
"  execute pathogen#infect()

" -----------------------
" Szerokosc tabów
" -----------------------
   set tabstop=2
   set shiftwidth=2
   set expandtab

" -----------------------
" Szerokosc tekstu
" -----------------------
"   set textwidth=105

" -----------------------
" Kolor linii do ToggleCC
" -----------------------
   highlight ColorColumn ctermbg=lightgrey

" -----------------------
" Numerowanie linii kodu:
" -----------------------
   set number

" -----------------------
" Rozroznianie i kolorowanie typu kodu na podstawie rozszerzenia
" -----------------------
  syntax on
  filetype plugin indent on
  hi Pmenu ctermbg=238 ctermfg=White 
  hi Comment ctermfg=grey
  hi SignColumn ctermbg=236
  hi YcmErrorSign ctermbg=124 ctermfg=black
  hi YcmWarningSign ctermbg=214 ctermfg=black
  hi SpellBad ctermbg=124 ctermfg=white
  hi SpellCap ctermbg=214 ctermfg=black

" -----------------------
" Rekursywne wyszukiwanie plików i autouzupełnianie nazw w pasku menu
" -----------------------
set path+=**
set wildmenu

" -----------------------
" Relatywne numerowanie linijek
" -----------------------
set rnu

" -----------------------
" Autouzupełnianie LaTeX
" -----------------------
"au FileType * execute 'setlocal dict+=~/.vim/words/'.&filetype.'.txt'
"set complete+=k

" -----------------------
" Ustawienie kodowania plików
" -----------------------
"setglobal encoding=utf-8
"setglobal fileencoding=utf-8
"setglobal termencoding=utf-8
"setglobal printencoding=utf-8
"scriptencoding utf-8
"set t_ut=

" -----------------------
" Vimdiff colorscheme
" -----------------------
"if &diff
"    let g:gruvbox_contrast_dark = "soft"
"    set t_Co=256
"    set background=dark
"    highlight Normal ctermbg=NONE
"    colorscheme gruvbox
"endif

" -----------------------
" Vim mouse support
" -----------------------
set mouse=a

" -----------------------
" Vim copy paste (kind of)
" -----------------------
set guioptions+=a

" -----------------------
" Vim alacritty support
" -----------------------
if !has('nvim')
  set term=xterm-256color
endif

" -----------------------
" Vim color hexcodes
" -----------------------
autocmd VimEnter * ColorHighlight

xnoremap <silent> <leader>y y:call system("wl-copy --trim-newline", @*)<cr>:call system("wl-copy -p --trim-newline", @*)<cr>

"nnoremap <silent> <leader>p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p

"nnoremap <silent> <leader>p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p

"################################################################################
" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
" let skip_defaults_vim=1
